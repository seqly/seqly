/**
 * <p><em>Seqly</em> (short for sequence library) contains
 * {@link org.bitbucket.seqly.Seq}, which extends
 * {@code Collection} and directly defines eager versions
 * of almost all {@code Stream} methods like
 * {@code map}, {@code filter} and {@code reduce}.
 */
package org.bitbucket.seqly;
